import unittest

if __name__ == "__main__":
    load = unittest.TestLoader()
    load.testMethodPrefix = "test"
    suite = load.discover('test_pr', pattern="test*.py")
    tests = unittest.TestSuite((suite, ))
    unittest.TextTestRunner(verbosity=2).run(tests)
